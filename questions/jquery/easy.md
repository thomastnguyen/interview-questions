### What is jQuery?

* jQuery is a framework that provides cross-platform DOM manipulation APIs.

### What problem(s) does jQuery solve?

* Resolves cross-platform discreprancies with DOM traversal, event handling, ajax, and animation.

### What does $(function () {}) do?

```js
document.addEventListener("DOMContentLoaded");
```

### What are selectors?

```js
// tag
$("a");

// id
$("#some-random-tag");

// class
$(".some-random-class");
```
