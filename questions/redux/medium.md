### Describe the state flow within an redux application

* An action is dispatched.
* Reducers "listen" to dispatched actions and update the state according.
* Components receive updated props.
