### What is redux?

* Redux is a transactional-based library that handles state synchronously.

### What problem(s) does redux solve?

### What is an action?

* An action is a form of transaction that communicate a change to the store.

### What is a reducer?

* A reducer responds to an action and updates the state based on the received action.

### What is dispatch?

* Dispatch is an API that allows the developer to send actions.

### What is a store?

* A store is the single source of truth within a redux application.

### How do you connect a component to the store?

* connect([mapStateToProps], [mapDispatchToProps])(Component)

### What is mapStateToProps?

* mapStateToProps reads state from the redux store and maps it to component props

### What is mapDispatchToProps?

* mapDispatchToProps map dispatch to action creaters and pass it as component props
