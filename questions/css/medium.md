### What is the difference between absolute position and relative position?

* An absolute position element is positioned relative to the first parent element that has a position other than static.
* A relative position element is positioned relative to its normal position.

### What is the rule or keyword used to define a style for a specific screen width

* @media or media query

### What is a FOUT or FOUC and what are some causes of each?

* FOUT: Flash of Unstyled Text. Possible cause is loading a non-system font after the DOM
* FOUC: Flash of Unstyled Content. Possible cause is loading CSS after the DOM is rendered

### What is meant by the statement "CSS is render blocking?" Why is this often a good thing?

* Render blocking means the browser won't render any content until the CSSOM is constructed.
* External CSS files are often loaded in the <head /> which blocks the rendering of the DOM. It's often a good thing because it will prevent a FOUC and a repaint of the DOM

### What are CSS vendor prefixes? Bonus: what is an easy way to add vendor prefixes to your CSS?

* Vendor prefixes are added to some non-standard or experimental CSS properties by the browser vendor. Once the behavior is standardized developers can remove the prefix
* Examples:

  * -webkit-
  * -moz-
  * -ms-

* Bonus Answer: Use a build tool like Autoprefixer/webpack

### Describe a pseudo class and give an example of how you might use one to add a font icon to an element

* :hover, :visited, :active, :link and :focus
* :before (and/or :after) is commonly used to add font icons to an element

```
.my-icon:before {
    font-family: iHerb-Font;
    content: "\e95a";
}
```
