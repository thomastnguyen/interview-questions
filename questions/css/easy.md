### What is CSS?

* CSS (Cascading Style Sheets) is a styling language.

### What are some ways to select elements?

* id

```css
#some-random-id {
}
```

* class

```css
.some-random-class {
}
```

* tag

```css
a {
}
```

### What are the different type of positions?

* static
* relative
* absolute
* fixed

### Describe the characteristics of a block, inline and inline-block element.

* inline elements

  * do not respect top and bottom margin/padding
  * do not respect height and width
  * do not cause a line break

* block elements

  * cause a line break so no elements can sit to their left or right
  * adhere to all margin, padding, height and width styles
  * just the line will wrap, not the whole element (seen with border or background color)

* inline-block elements
  * are still inline meaning other elements can be on their left or right
  * respect height, width and top & bottom margin/padding
  * the whole element will wrap (it's border/background color will surround the whole element rather than just the line)

### Explain absolute positioning and how it works.

* Absolute position takes the element out of the dom workflow
* It will be positioned to the nearest positioned ancestors
* If there is none, it will be positioned to the document body.

### What is CSS specificity and what is it used for?

* Specificity is the means by which browsers decide which CSS property values are the most relevant to an element and, therefore, will be applied
