### What is the plus + character and how can it be used?

* Adjacent sibling combinator. It will select any immediate siblings.

```html
<p class="target"></p>
<p>this should be red</p>
```

```css
.target + p {
  color: red;
}
```

### What is the tilde ~ character and how can it be used?

* The general sibling combinator. It separates two selectors and matches all siblings following the first element ( but not necessarily immediately following).

```html
<p class="target"></p>
<p>should be red</p>
<p>should be red</p>
```

```css
.target ~ p {
  color: red;
}
```

### How do you select a div whose data-test attribute is "select-me?"

```css
div[data-test="select-me"] {
}
```

### What are keyframes and how might you use them?

* Keyframes in CSS are used to create steps in CSS animations
* Set each waypoint with "from" and "to," percentages or both ("from" is the same as 0% and "to" is the same as 100%)

```css
p {
  animation: 2s ease-in-out my-animation;
}

@keyframes my-animation {
  /* 0% or "from" */
  from {
    margin-left: 100%;
    background: red;
    width: 100%;
  }
  /* 100% or "to" */
  to {
    margin-left: 0%;
    background: yellow;
    width: 100%;
  }
}
```
