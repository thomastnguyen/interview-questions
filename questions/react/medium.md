### What is the difference between React and ReactDOM?

* React is the library that allows the developer to define the components.
* ReactDOM is the library that allows the developer to render the component into a browser environment.

### What is reconciliation?

* React uses a differential algorithm that calculates which DOM nodes need to be updated to efficiently update and render components.

### How does data travel in React?

* Unidirectionally from parent to child.

### What is context?

* Context is an API that allows components to access shared data.

```js
const ThemeContext = React.createContext("light");

class ThemeProvider extends React.Component {
  state = { theme: "light" };

  render() {
    return (
      <ThemeContext.Provider value={this.state.theme}>
        {this.props.children}
      </ThemeContext.Provider>
    );
  }
}

class ThemedButton extends React.Component {
  render() {
    return (
      <ThemeContext.Consumer>
        {theme => <Button theme={theme} />}
      </ThemeContext.Consumer>
    );
  }
}
```
