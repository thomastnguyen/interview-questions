### What is React?

* React is a virtual-dom library that provides a flat API to create reusable web components.

### What problem(s) does React solve?

* It reduces the burden of context switching by allowing the developer to implement the view, business logic, and styling of an UI component with one language.
* It efficiently updates and renders components based on the developer's code and internal heuristics.
* It allows cross-platform code.

### What is the difference between Functional Component and Class Component?

* Class component allows the developer to add in lifecycle method(s) and manage state.

```js
const statelessFunctionalComponent = () => (
    <div>this is a stateless functional component</div>
);

const ClassComponent extends React.Component {
    // lifecycle
    componentDidMount() {

    }

    handleStateChange() {
        this.setState({

        });
    }

    render() {
        return (
            <div>this is a class component</div
        )
    }
}
```

### What is the difference between a Container (smart) and a Component (dumb)?

* A component is stateless, while a container is stateful.

### What are refs and why are they important?

* Refs are an escape hatch that allows the developer direct access to a DOM element.
* The preferred usage is to pass a callback function.

```js
class Button extends React.Component {
  // node reference
  button = null;

  render() {
    return <button ref={node => (this.button = node)}>this is a button</button>;
  }
}
```

### What are keys and why are they important?

* Keys help React keep track of what items have changed, been added, or been removed from a list.

```js
class List extends React.Component {
  state = {
    list: []
  };

  render() {
    return <ul>{list.map((l, key) => <li key={key}>l</li>)}</ul>;
  }
}
```

### What is the difference between a controlled component and an uncontrolled component?

* A controlled component is a component where React controls the source of truth.
* An uncontrolled component is a component where the DOM holds the source of truth.

### What are the lifecycle methods provided by React?

* componentWillMount
* componentWillUnmount
* componentWillReceiveProps
* shouldComponentUpdate
* componentWillUpdate
* componentDidUpdate

### What is the difference between building React in production vs development?

* When React is build in production mode, internal validation logic are stripped from the final version.

### What API do you use to signal that the state has changed?

* setState

### What are the two ways you can call setState and how are they different?

* The second version allows the developer to deterministicly update state.

```js
// setState(object, callback)
this.setState({}, () => {});

// setState((prev, props) => object, callback);
this.setState((state, props) => ({}), () => {});
```
