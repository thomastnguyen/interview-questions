### How do you declare a function?

```js
function() {

}
```

```js
var bar = function() {};
```

### What is the difference between the two variable declaration?

```js
// global variable
foo = 1;

// closest scope, possibly global
var bar = 1;
```

### What are the looping structures in JavaScript?

```js
// for-loop
for (let i = 0; i < 10; i++) {}

// while
while (true) {}

// do-while
do {} while (true);
```

### What is chaining?

* Chaining is when the developer calls one method after another on an object.

```js
[].map(function() {});
forEach(function() {});
```

### What are asynchronous callbacks?

* Async callbacks do not block or wait for the api call to return from the server. The execution script will continue.

```js
// resolve is an example of an asynchronous callback function
var promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve("some-random-data");
  }, 5000);
});
```
