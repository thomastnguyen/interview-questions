### Explain how this works in JavaScript?

* At the time of execution of every function, JavaScript sets this to the current execution context.

### How would you implement chaining?

```js
class Array {
  map() {
    return this;
  }

  forEach() {
    return this;
  }
}

new Array().map().forEach();
```
