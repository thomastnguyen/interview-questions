### What is IIFE?

* IIFE stands for Immediately-Invoked Function Expression.
* It allows the developers to create a closure that runs on load.

```js
(function() {})();
```

### What are two programming paradigms that are important for JavaScript developers?

* Protoypal inheritance
* Functional programming

### What is string interpolation?

* String interpolation is the process of evaluating string literal containing one or more placeholders.

```js
let value = 1;
// this is an awesome message: 1
let message = `this is an awesome message: ${value}`;
```

### What is async/await?

* Async/await are synthax sugar that allows you to write asynchronous code synchronously.

```js
var getDataFromServer = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("some-random-data");
    }, 5000);
  });
};

(async function() {
  // data = "some-random-data"
  var data = await getDataFromServer();
})();
```

### What is spread?

* Spread is a syntax sugar that allows you to merge values together.

```js
// object
let a = {
  a: 1
};

let b = {
  b: 2
};

// { a : 1, b : 2}
let c = {
  ...a,
  ...b
};

// array
let a = [1];
let b = [2];

// [1, 2]
let c = [...a, ...b];
```

### What is the difference between == and ===?

* == is a loose equality operator which returns true if the values are alike
* === is a strict equality operator which returns true if the values are the same

```js
// true
1 == "1";

// false
1 === "1";
```

### What is hoisting ?

* Hoisting is a JavaScript mechanism where variables and function declarations are moved to the top of their scope before code execution.
