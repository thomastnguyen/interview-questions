### What is render-blocking?

* The browser won't render any processed content until the render blocking resource finishes.

### What is considered as render-blocking?

* JS
* CSS

### What is DOMContentLoaded?

* DOMContentLoaded is the point in time when both the DOM is ready and there are no stylesheets that are blocking JavaScript execution.

### What is Load?

* Load is the point in time when all resources have finished loading.

### What is the difference between localStorage and sessionStorage?

* both: store data on client side
* localStorage: no expiration date, cleared through JS, cleared through browser
* sessionStorage: expires when browser closes

### What is the difference between GET and POST?

* both: HTTP requests
* GET: retrieves data, size limit
* POST: submits data through body to be processed
