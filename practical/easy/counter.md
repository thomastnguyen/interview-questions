### Requirements

* layout
* feature: increment, decrement, reset
* extra: persistance

```js
import React from "react";

class Counter extends React.Component {
  constructor(props) {
    super(props);

    const count = Number(sessionStorage.get("counter")) || 0;

    this.state = {
      count
    };
  }

  handleUpdate = value => {
    this.setState(
      state => ({
        count: state.count + value
      }),
      () => {
        sessionStorage.set("counter", this.state.count);
      }
    );
  };

  render() {
    return (
      <div>
        <div>Counter: {this.state.count}</div>
        <button onClick={() => this.handleUpdate(1)}>increment</button>
        <button onClick={() => this.handleUpdate(-1)}>decrement</button>
        <button onClick={() => this.handleUpdate(-this.state.count)}>
          reset
        </button>
      </div>
    );
  }
}
```
